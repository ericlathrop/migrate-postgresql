# migrate-postgresql
Make changes to your PostgreSQL schema & data in a structured way.

## migrate(migrationDirectory, direction, pool)

Run all migrations found in `migrationDirectory`.
`direction` is either `"up"` or `"down"`.
`pool` is an instance of the `Pool` class from the `pg` module.

## create(migrationDirectory, name)

Create a new timestamped migration file in `migrationDirectory`.
