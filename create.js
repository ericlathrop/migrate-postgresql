const fs = require("fs");
const { COPYFILE_EXCL } = fs.constants;
const path = require("path");
const util = require("util");

const copyFile = util.promisify(fs.copyFile);

module.exports = create;

async function create(migrationDirectory, name) {
  const src = path.join(__dirname, "new-migration-template.js");

  const filename = `${currentTimestamp()}-${name}.js`;
  const dest = path.join(migrationDirectory, filename);

  await copyFile(src, dest, COPYFILE_EXCL);
};

function currentTimestamp() {
  return new Date()
    .toISOString()
    .replace(/\..+/, "")
    .replace(/[\D]/g, "");
}

