const fs = require("fs");
const path = require("path");
const util = require("util");
const storage = require("./storage");

const readdir = util.promisify(fs.readdir);

module.exports = migrate;

async function migrate(migrationDirectory, direction, pool) {
  const migrations = await readdir(migrationDirectory);
  migrations.sort();

  await storage.setup(pool);

  var ran = [];
  for (var migration of migrations) {
    if (await executeMigration(migrationDirectory, migration, direction, pool)) {
      ran.push(migration);
    }
  }

  return ran;
};

async function executeMigration(migrationDirectory, name, direction, pool) {
  const migration = require(path.resolve(migrationDirectory, name));
  const maybeRunMigration = direction === "down" ? maybeRunMigrationDown : maybeRunMigrationUp;
  return await withClient(pool, client => maybeRunMigration(client, name, migration[direction]));
};

async function maybeRunMigrationUp(client, name, migration) {
  if (!await storage.hasMigrationBeenRun(client, name)) {
    await migration(client);
    await storage.recordMigration(client, name);
    return true;
  }
  return false;
};

async function maybeRunMigrationDown(client, name, migration) {
  if (await storage.hasMigrationBeenRun(client, name)) {
    await migration(client);
    await storage.forgetMigration(client, name);
    return true;
  }
  return false;
};

async function withClient(pool, fn) {
  // note: we don't try/catch this because if connecting throws an exception
  // we don't need to dispose of the client (it will be undefined)
  const client = await pool.connect();

  try {
    await client.query("BEGIN");
    var ret = await fn(client);
    await client.query("COMMIT");
    return ret;
  } catch (e) {
    await client.query("ROLLBACK");
    throw e;
  } finally {
    client.release();
  }
};
