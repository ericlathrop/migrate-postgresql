async function setup(client) {
  await client.query("CREATE TABLE IF NOT EXISTS migrations (migration varchar(255) NOT NULL)");
  await client.query("CREATE UNIQUE INDEX IF NOT EXISTS migrations_unique_name ON migrations (migration)");
};

async function recordMigration(client, name) {
  await client.query("INSERT INTO migrations (migration) VALUES ($1)", [name]);
};

async function forgetMigration(client, name) {
  await client.query("DELETE FROM migrations WHERE migration = $1", [name]);
};

async function hasMigrationBeenRun(client, name) {
  const { rows } = await client.query("SELECT 1 FROM migrations WHERE migration = $1", [name]);
  return rows.length === 1;
};

module.exports = {
  setup,
  recordMigration,
  forgetMigration,
  hasMigrationBeenRun
};
